# Ejercicio 1 MERN OB



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/octavioiorio99/ejercicio-1-mern-ob.git
git branch -M main
git push -uf origin main
```

## Explicaciones Lección 1

## Dependencias
@types/express => contienes los tipos de definiciones de express

@types/jest => contienes los tipos de definiciones de jest

@types/node => contienes los tipos de definiciones de node

@typescript-eslint/eslint-plugin => proporciona reglas en el código TypeScript

concurrently => ejecuta varios comandos de manera concurrida, ejemplo: 'npm run watch-js & npm run watch-less' pero mejor

eslint => es una herramienta donde identifica y reporta patrones encontrados en código JavaScript

eslint-config-standard-with-typescript => se basa en el 'eslint-config-standard' y contienes reglas específicas de TypeScript

eslint-plugin-import => corrige o previene en problemas de falta de ortografía en archivos o imports

eslint-plugin-n => reglas adicionales de eslint para Node.js

eslint-plugin-promise => fuerza en las buenas prácticas para promesas de JavaScript

jest => para testear nuestro código de JavaScript

nodemon => reinicia la aplicación Node.js cuando hay un cambio en nuestro proyecto, por ejemplo: una modificación de un texto o mover un archivo a otro directorio

serve => para servirte una web estatica

supertest => para proveer un nivel alto de abstracción para el testeo HTTP

ts-jest => para testear nuestro código de TypeScript

ts-node => motor de ejecución de TypeScript para Node.js

typescript => un lenguaje para la escala de aplicación JavaScript

webpack => un módulo con el propósito de agrupar JavaScript

webpack-cli => un módulo que proporciona una flexibilidad de una serie de comandas para los desarrolladores para incrementar la velocidad de configuración del webpack

webpack-node-externals => proporciona una forma de excluir dependencias de los paquetes de salida

webpack-shell-plugin => le permite ejecutar cualquier comando shell antes o después de las compilaciones de webpack

## Scripts
"build" => construir el archivo tsconfig.json

"start" => iniciamos el proyecto mediante index.js y no con index.ts

"dev" => se nos iniciará nuestro proyecto y en el propio terminal con nodemon nos reiniciará el servidor cada vez que llegué una modificación o accedamos a una ruta

"test" => probar la herramienta jest para ver errores que ten

"serve:coverage" => nos abrirá una página que contiene la herramienta jest viendo los errores que hemos tenido

## Variables
app => para iniciar el aplicación express

dotenv => genera todos los cambios necesarios en el objeto process a través de su método config(), consumiendo el archivo . env
