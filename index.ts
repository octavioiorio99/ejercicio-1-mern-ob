import express, { Express, Request, Response } from "express";
import dotenv from 'dotenv';
import bodyParser from "body-parser";


// Configuration the .env file
dotenv.config();

// Create Express APP
const app: Express = express();
const port: string | number = process.env.PORT || 8000;
app.use(bodyParser.urlencoded({ extended: true }))

// Define the first Route of APP
app.get('/', (req: Request, res: Response) => {
    // Send Hello World
    //res.send('Ejercicio 1 OB')
    res.status(200).json({
        data: {
            message: "Goodbye, world"
        }
    });
});

// Define the first Route of APP
/*app.get('/hello', (req: Request, res: Response) => {
    // Send Hello World
    res.send('Welcome to GET Route: ¡Hello!')
});*/
app.post('/hello', function (req: Request, res: Response) {
    
    var edad = req.body || '';
    res.json(edad)

});

// Execute APP and Listen Requests to PORT
app.listen(port, () => {
    console.log(`EXPRESS SERVER: Running at http://localhost:${port}`);
});